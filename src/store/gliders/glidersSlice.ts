import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { glidersApi } from '../../api/api';

export const getGliders = createAsyncThunk('gliders', async (_, thunkAPI) => {
  try {
    return await glidersApi.getGliders();
  } catch (e: any) {
    return thunkAPI.rejectWithValue(e.response.data);
  }
});

const glidersSlice = createSlice({
  name: 'gliders',
  reducers: {},
  initialState: {
    gliders: [] as GliderType[],
    isError: false,
    isLoading: false,
    message: ''
  } as InitialStateType,
  extraReducers: builder => {
    builder.addCase(getGliders.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(getGliders.fulfilled, (state, action: any) => {
      state.isLoading = false;
      state.gliders = action.payload;
      console.log(state.gliders);
    });
    builder.addCase(getGliders.rejected, (state, action: any) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload.message;
      state.gliders = [];
    });
  }
});

export const glidersReducer = glidersSlice.reducer;

export type GliderType = {
  _id?: string;
  name: string;
  price: number;
  description: string;
  addDescription: string;
  range: number;
  gliderImg: string;
  __v?: number;
};

export type InitialStateType = {
  gliders: GliderType[];
  isError: boolean;
  isLoading: boolean;
  message: string;
};
